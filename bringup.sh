#!/bin/bash
while [ true ]; do
  if ls /dev/ttyACM* 1> /dev/null  2>&1 || ls /dev/ttyUSB* 1> /dev/null  2>&1 ;then
    break
  else
    echo "waiting for the uart..."
  fi
done

cmds=(
       "ros2 launch hnurm_bringup compose.launch.py"
    )
for cmd in "${cmds[@]}"
do
    echo Current CMD : "$cmd"
    gnome-terminal -- bash -c "cd $(pwd);source /home/rm/hnuvision/install/setup.bash;$cmd;exec bash;"
    sleep 0.2
done