#include "hnurm_detect/detect_node.hpp"

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::NodeOptions options;
    options.use_intra_process_comms(true);

    RCLCPP_INFO(rclcpp::get_logger("detect_node"), "detect node started");
    rclcpp::spin(std::make_shared<hnurm::DetectNode>(options));

    return 0;
}