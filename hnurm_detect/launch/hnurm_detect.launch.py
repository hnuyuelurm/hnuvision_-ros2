import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    detect_dir = get_package_share_directory('hnurm_detect')
    params_file = LaunchConfiguration('params_file')

    print(detect_dir)

    return LaunchDescription([
        DeclareLaunchArgument(
            'params_file',
            default_value=os.path.join(detect_dir, 'params', 'default.yaml'),
            description='detect params file'
        ),
        Node(
            package='hnurm_detect',
            executable='hnurm_detect_node',
            output='screen',
            parameters=[params_file]
        ),
    ])
