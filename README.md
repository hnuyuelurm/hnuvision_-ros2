
# 欢迎

这里是湖南大学robomaster跃鹿战队视觉运维手册。

作者：徐涵(underturrets@163.com)


# 环境配置

1. miniPC基础设置

安装Ubuntu系统，最小安装，设置语言为英文，用户名和密码详询其他队员。


2. 切换软件源

直接在系统中切换即可

3. 安装软件和依赖

此处有一键配置脚本，如果其中有的链接失效，可以自行使用搜索引擎下载最新资源。**如果系统语言是中文，有的指令会执行失败。**
```shell
#!/bin/sh
#基础环境配置
sudo apt-get -y upgrade
sudo apt-get -y install git cmake openssh-server libeigen3-dev  libopencv-dev
sudo apt-get -y install g++
#下载MVS到~/Download目录，并解压安装
wget -P ~/Downloads/ https://www.hikrobotics.com/cn2/source/support/software/MVS_STD_GML_V2.1.2_231225.zip
unzip ~/Downloads/MVS_STD_GML_V*.zip
sudo dpkg --install ~/Downloads/MVS-2.1.2_x86_64*.deb
#相机驱动文件复制
sudo cp $(pwd)/hnurm_camera/lib/linux/* /usr/lib/
#下载nomachine到/usr目录并解压安装
wget -P ~/Downloads/ https://download.nomachine.com/download/8.11/Linux/nomachine_8.11.3_4_amd64.deb
sudo dpkg --install ~/Downloads/nomachine*amd64.deb

#其他依赖项
sudo apt -y install ros-humble-sophus
sudo apt -y install ros-humble-libg2o
sudo apt -y install ros-humble-camera-info-manager
sudo apt -y install ros-camera-calibration
sudo apt -y install ros-humble-camera-calibration
rosdep install -i --from-path src --rosdistro $ROS_DISTRO -y
```

4. 安装ROS2 humble

此处有官网的安装步骤，已经制作成一键安装脚本。

- 可能会出现无法定位软件包的问题，可以使用aptitude再次安装。
- 可能会出现`sudo add-apt-repository universe`添加源失败问题，需要手动设置可信源。
- 如果嫌麻烦，可以使用`wget http://fishros.com/install -O fishros && . fishros`的一键安装指令，但这是非官方提供。
```shell
#!/bin/bash
#Make sure you have a locale which supports UTF-8. If you are in a minimal environment (such as a docker container), the locale may be something minimal like POSIX. We test with the following settings. However, it should be fine if you’re using a different UTF-8 supported locale.
sudo apt update && sudo apt -y install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8
#You will need to add the ROS 2 apt repository to your system. First ensure that the Ubuntu Universe repository is enabled.
sudo apt -y install software-properties-common
sudo add-apt-repository universe
#Now add the ROS 2 GPG key with apt.
sudo apt update && echo $PASSWORD |sudo apt -y install curl
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
#Then add the repository to your sources list.
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
#Update your apt repository caches after setting up the repositories.
sudo apt update
#ROS 2 packages are built on frequently updated Ubuntu systems. It is always recommended that you ensure your system is up to date before installing new packages.
sudo apt -y upgrade
#Desktop Install (Recommended): ROS, RViz, demos, tutorials.
sudo apt -y install ros-humble-desktop
#ROS-Base Install (Bare Bones): Communication libraries, message packages, command line tools. No GUI tools.
sudo apt -y install ros-humble-ros-base
#Development tools: Compilers and other tools to build ROS packages
sudo apt -y install ros-dev-tools
#Set up your environment by sourcing the following file.
echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
```

5. 修改ip
修改miniPC的ipv4地址如下：
- 手动 
- 地址：192.168.1.1
- 掩码：255.255.255.0
- 网关：192.168.1.0

使用nomachine是为了避免每次调试都要连接键鼠，而且在比赛时也不可能带键鼠去调试！

关于组局域网连接nomachine的方法原理的进一步阐释，可以参考[这里](https://blog.csdn.net/qq_50791664/article/details/135578810)。

6. 创建路径`/home/rm/hnuvision/src`，将本项目目录下所有文件（包括隐藏文件）复制或剪切到这个路径，然后使用`colcon build`编译，编译失败多半是依赖没装好，回到之前再补充安装即可。

7. 设置自启动
在启动应用程序中添加
```shell
bash /home/rm/hnuvision/src/bringup.sh
```

8. 串口权限设置
将当前用户加入`dialout`用户组，避免每次都要手动赋予权限。
```shell
sudo usermod -aG dialout rm
```


此外，关于IDE的安装自凭喜好即可。

# 概览

## 功能包
本框架基于ROS平台设计了6个功能包，如下：

1. hnurm_uart：负责与下位机的通讯
2. hnurm_camera：调用相机
3. hnurm_detect：检测装甲
4. hnurm_armor：预测装甲的运动并进行补偿
5. hnurm_bringup：启动其他全部节点并适配双云台
6. hnurm_interfaces：存储所有消息类型

## 自瞄流程
1. 启动`hnurm_uart`节点，可以自动识别串口名字并尝试读写数据。
2. 启动`hnurm_camera`节点，可以根据参数文件中的相机id打开相机获取图像。
3. 启动`hnurm_detect`节点，可以对相机图像进行装甲板的提取。
4. 启动`hnurm_armor`节点，可以装甲板进行预测和弹道的计算，并将计算结果发送给`hnurm_uart`。
5. 启动`hnurm_bringup`功能包中的`compose_node`节点，可以一次性启动如上所有节点。对于哨兵这样的双云台机器人来说，需要

# 标定脚本执行方法
1. 准备好标定板，根据标定板的方格数量、方格面积、方格类型修改`hnurm_camera/launch/calib.launch.py`文件。
```python
    # execute `ros2 run camera_calibration cameracalibrator --size --square
    camera_calibration_node = ExecuteProcess(

        cmd=['ros2', 'run', 'camera_calibration', 'cameracalibrator', '--size', '11x8', '--square', '0.03',
             '--no-service-check'
             ],
        output='screen'
    )
```
2. 运行此文件，移动标定板进行标定。标定距离不要超过2m；标定时，标定板俯仰角上下15°左右，偏航角不限；图像边缘也要照顾到。
3. 标定文件存储在`/tmp`下，解压缩并打开文件，把相机内参矩阵手动复制到`hnurm_aromor/data`下任意一个标定文件下，并顺便依据相机id修改标定文件名字。


# 串口问题
## 解包顺序
首先，检查电控发送的俯仰角数据和偏航角数据是否正确对应。惯性传感器坐标系如下：
y==前，z==上，x==右；
y对应roll，z对应yaw，x对应pitch；
右手定则为正方向

如果对应错误，可以在`protocol.cpp`的`encode`和`decode`函数中修改编解码顺序来修正。

# 装甲板识别问题
## 灯条识别正常，但是没有构成装甲板
- 查看`hnurm_detect/params/default.yaml`中ignore_classes参数，可能因为是识别成`negative`被筛除了
```yaml
    ignore_classes:
      - "2"
#      - negative
#      - outpost
#      - base
```

- `hnurm_detect/src/detector.cpp`中的`isArmor`函数把它筛去了，调整参数试试。

## 解决数字识别网络的误识别问题：
1. 在适当的距离重新对焦并标定
2. 调整光圈，调整CameraParam.yaml的曝光时间和增益
3. 调整二值化阈值


# 解算问题
## PnP解算出的相对位置误差较大
1. 检查大小装甲板的宽度和高度是否设置正确
```cpp
// 小型护甲的灯条组成矩形的宽度，单位：毫米
static constexpr float SMALL_ARMOR_WIDTH = 135;
// 小型护甲的灯条组成矩形的高度，单位：毫米
static constexpr float SMALL_ARMOR_HEIGHT = 55;
// 大型护甲的灯条组成矩形的宽度，单位：毫米
static constexpr float LARGE_ARMOR_WIDTH = 225;
// 大型护甲的灯条组成矩形的高度，单位：毫米
static constexpr float LARGE_ARMOR_HEIGHT = 55;
```

2. 检查相机成像是否清晰（即焦距是否合适），光圈是否调整合适。**每次调整完焦距或者光圈后都需要重新标定**。

3. 检查`hnurm_armor/params/default.yaml`中的相机偏移参数是否正确。应该设置为相机光心在惯性传感器坐标系下的坐标:相机镜头方向为y轴正方向，天空为z轴正方向。
```yaml
      solver:
        calib_info_path: "src/hnurm_armor/data/"
        # bias to imu
        camera_bias_z: 0
        camera_bias_y: 0
        camera_id: 2BDF67015861   
```
5. 检查在装甲板在静止时能否瞄准，并进行开火测试。在`hnurm_armor/params/default.yaml`中添加偏移参数以消除物理误差，确保静止目标可以正中靶心。
```yaml
    compensator:
      gravity: 9.8
      friction_coeff: 0.02
      bullet_speed_bias: -0.5
      channel_delay: 0.01 # s
      yaw_bias: 0.0
      pitch_bias: 0.0 # dirty param 
```

# 预测问题
## 查看channel_delay是否设置正确
channel_delay这个时间定义为电控受到开火指令后转动拨盘的延迟，这个参数会严重的影响预测的准度。对于步兵而言，比赛时几乎都是连发，可以设置为0.05；对于英雄而言，就需要去询问电控组的同学。此外，调整方式如下：
1. 当弹道晚于目标，那么channel_delay过小，程序认为子弹几乎可以立即发射，但现实是子弹发射延迟更大，所以弹道慢了。应该增大channel_delay
2. 弹道早于目标，应该减小channel_delay

## 查看预测角速度是否正确
预测的角速度是能否击中目标的关键，这通常与曝光时间和增益有所关联。曝光时间增加可以获得更多颜色信息，但会降低帧率。

# 完整调试流程
1. 调整光圈、焦距，标定（四个角不要有畸变），对应地方修改相机id和参数（宽高、像素格式）
2. 根据比赛场地调整曝光和增益，主要是曝光
3. 调整装甲板识别参数、相机光心与惯性传感器的相对位置、摩擦轮中心与惯性传感器的相对位置
4. 静止击打装甲板确保可以正中，主要调整bias_yaw。bias_pitch默认就是0，几乎不用调整，如果pitch不准，优先去调整摩擦系数，实在不行的话再去调bias_pitch
5. 反小陀螺调参，主要调延迟时间，要是角速度非常不准就重新适当调整曝光和增益
6. 调整开火策略，决定自瞄发射的速度。

# foxglove调试指南
1. 安装依赖
```shell
sudo apt install ros-$ROS_DISTRO-foxglove-bridge
```
2. 启动节点
```shell
$ ros2 launch foxglove_bridge foxglove_bridge_launch.xml
```
3. 打开应用即可


