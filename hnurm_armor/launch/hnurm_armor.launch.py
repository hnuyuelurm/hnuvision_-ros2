import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    armor_dir = get_package_share_directory('hnurm_armor')
    params_file = LaunchConfiguration('params_file')

    return LaunchDescription([
        DeclareLaunchArgument(
            'params_file',
            default_value=os.path.join(armor_dir, 'params', 'default.yaml'),
            description='armor params file'
        ),
        Node(
            package='hnurm_armor',
            executable='hnurm_armor_node',
            output='screen',
            parameters=[params_file]
        ),
    ])
