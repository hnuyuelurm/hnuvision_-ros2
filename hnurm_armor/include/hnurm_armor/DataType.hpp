
#ifndef DATATYPE_H
#define DATATYPE_H

#include <opencv2/opencv.hpp>

// #include "easylogging++.h"

namespace hnurm
{
// compulsory class enumerate
enum class WorkMode : int
{
    auto_shoot = 0,  // hero,infantry
    auto_sbuff = 1,  // infantry
    auto_bbuff = 2,  // infantry
};

enum class SelfColor : int
{
    color_none = 0,
    red        = 1,
    blue       = 2,
};

enum class BulletSpeed : int
{
    bulle_speed_none = 0,
    hero10           = 10,
    hero16           = 16,
    infantry15       = 15,
    infantry18       = 18,
    infantry30       = 30  // sentry have the same bullet speed
};

enum class TargetState : int
{
    lost_target = 0,
    converging  = 1,
    fire        = 2,
};

enum class TargetType : int
{
    none      = 0,
    hero      = 1,
    engineer  = 2,
    infantry3 = 3,
    infantry4 = 4,
    infantry5 = 5,
    outpost   = 6,
    sentry    = 7,
    base      = 8,
};

/**
 * @brief 相机输出,传递给detector进行识别
 *
 */
struct ImgInfo
{
    ImgInfo() = default;

    explicit ImgInfo(cv::Mat &raw_img)
    {
        time_stamp = std::chrono::steady_clock::now();
        img        = raw_img.clone();
    }

    cv::Mat                               img;
    std::chrono::steady_clock::time_point time_stamp;
};

/**
 * @brief 由Processor输出的目标信息,传递给Compensator进行补偿计算
 *
 */
struct TargetInfo
{
    TargetInfo()
    {
    }

    TargetState state;
    TargetType  type;

    float x {}, y {}, z {};
    float vx {}, vy {}, vz {};
    float w_yaw {}, yaw {};
    float radius_1 {}, radius_2 {};
    float dz {};
};

/**
 * @brief 由Compensator输出的补偿后的目标信息,传递给serial进行发送
 *
 */
struct VisionSendData  //: public el::Loggable
{
    VisionSendData() = default;

    VisionSendData(TargetState state_, TargetType type_, float pitch_, float yaw_)
        : state(state_),
          type(type_),
          pitch(pitch_),
          yaw(yaw_)
    {
    }

    TargetState state {};
    TargetType  type {};

    float pitch = 0;
    float yaw   = 0;  // formatted in float data

    // virtual void log(el::base::type::ostream_t &os) const
    // {
    //     os << "pitch: " << pitch << "yaw: " << yaw << "target state" << (int)state;
    // }
};

/**
 * @brief 由serial接收到的数据,传递给Processor用于计算装甲板的位姿
 *
 */
struct VisionRecvData  //: public el::Loggable
{
    VisionRecvData() = default;

    VisionRecvData(SelfColor self_color_, WorkMode mode_, BulletSpeed speed_, float pitch_, float yaw_, float roll_)
        : self_color(self_color_),
          mode(mode_),
          speed(speed_),
          pitch(pitch_),
          yaw(yaw_),
          roll(roll_)
    {
    }

    SelfColor   self_color;  // formatted in flag_register, lower 4 bits of low 8 bits
    WorkMode    mode;        // formatted in flag_register, lower 4 bits of high 8 bits
    BulletSpeed speed;       // formatted in flag_register, higher 4 bits of high 8 bits

    float pitch;
    float yaw;
    float roll;

    // // 重载<<运算符
    // virtual void log(el::base::type::ostream_t &os) const
    // {
    //     os << "self_color: " << static_cast<int>(self_color) << " mode: " << static_cast<int>(mode) << " speed: " <<
    //     static_cast<int>(speed) << " pitch: " << pitch << " yaw: " << yaw << " roll: " << roll;
    // }
};

enum class ArmorsNum
{
    NORMAL_4  = 4,
    BALANCE_2 = 2,
    OUTPOST_3 = 3,
    BASE_1 = 1
};

}  // namespace hnurm

#endif  // !DATATYPE_H